#4.1
#select maker
#from Product
#where maker in (select maker from Laptop)

#4.2
#select distinct maker
#from Product
#where(maker = any (select  maker from Product where type='PC'))
#and type='Laptop';

#4.3
#select maker
#from Product
#where (maker = any (select maker from PC ))

#4.4
#select maker
#from Product
#Where (maker in (select maker from Laptop))

#4.5
#select maker
#from Product
#where maker = all (select maker from PC )

#4.6
#select maker
#from Product
#where maker = any (select maker from PC, Laptop )

#5.1
#select maker
#from Product
#where exists (select maker from PC)

#5.2
#select maker
#from Product
#where exists (select speed from PC where speed >='750')

#5.3
#select maker
#from Product, Laptop
#where exists (select speed from PC where speed >='750')

#5.4
#select maker
#from Product
#where exists (select speed from PC)    

#6.1
#select AVG(price) middle
#from Laptop

#6.2
#select ('Код: '+ str(code,2)) as code,
#('Модель : '+ str(model,4)) as model,
#('Швидкість : '+ str(speed,3)) as speed,
#('ОЗП : '+ str(ram,3)) as ram,
#('Жорст диск : '+ str(hd,2)) as hd,
#('Оптич привід:'+ cd) as cd,
#('Ціна:'+str(price,3)) as price 
#from PC

#6.3
#select date, convert( tinyint, date, int)as [YYYY.MM.DD])
#from Income

#7.1
#select max(price),model
#from Printer

#7.2
#select type, model, max(speed)
#from Laptop
#where exists (select max(speed) from PC)

#9.1
#SELECT DISTINCT p.maker, 
#CASE WHEN (SELECT count(model)
#FROM PC 
#WHERE model IN (SELECT P2.model FROM product P2 WHERE P2.maker=P.maker ) ) 
#THEN 'YES('+ CAST ((SELECT count (model)FROM PC WHERE model IN (SELECT P2.model FROM product P2 WHERE P2.maker = P.maker)) AS nvarchar)+')' 
#ELSE 'NO' END pc
#FROM product P 
#GROUP BY P.maker

#10.2
#select 'PC' type, model, MAX(price) maxprice 
#from PC 
#group by  model union select 'Printer' type, model, MAX(price) maxprice from Printer 
#group by  model union select 'Laptop' type, model, MAX(price) maxprice from Laptop 
#group by  model
