
#create schema if not exists university default character set utf8 ;
#use university ;

#create table students (
#id int auto_increment primary key,
#name_surname char(20) not null,
#autobio varchar(100) not null,
#year_of_entry date not null,
#yeat_of_birth date not null,
#home_address text(30) not null
#)ENGINE InnoDB AUTO_INCREMENT 10;

#create table if not exists groups (
#group_cipher int  primary key,
#specialty text(30) not null
#);

#create table if not exists progress (
#rating int primary key,
#module_1 int,
#module_2 int,
#mark_100 varchar(10),
#mark_5 varchar(10),
#bursary int,
#specialty text(30) not null
#);


#create table if not exists schedule (
#semester int primary key,
#lecturer text(30) not null,
#subject text(30) not null 
#);


